package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ClaimHistoryHasDetailsOfClaimIn {

	private String claimOpenDTTM;
	private String claimStatusRk;
	private String claimStatusDesc;
	private String claimReferenceID;
	private String admissionDTTM;
	private String claimTypeCd;
	private String claimEndAMT;
	private String claimApprovalDTTM;
	private String dischargeDTTM;
	private String letterCD;
	private String claimCurrencyCD;
	private String key;
	private String claimStatusCD;
	private String planCd = "";
	private String remark = "";
}
