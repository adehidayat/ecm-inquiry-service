package id.co.axa.ecm.inquiry.service;

import javassist.NotFoundException;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 19, 2021
 * Time: 8:32:09 AM
 */
public interface InquiryInformationService {

	String getCustomerDataByPolicy(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getCustomerPolicyData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getCustomerProfileData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getPremiumBreakdownData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getInsuredDetailData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getBeneficiaryData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getFundDetailData(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getTransactionStatus(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getPaymentInfo(String body, String accessToken, String regid, String requestId) throws NotFoundException;
	String getClaimHistory(String body, String accessToken, String regid, String requestId) throws NotFoundException;
}
