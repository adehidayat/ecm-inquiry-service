package id.co.axa.ecm.inquiry.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: Apr 5, 2021
 * Time: 9:13:50 AM
 */
@Entity
@Table(name="users_group")
@Data
public class UsersGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="regional_code")
    private String regionalCode;

    @Column(name="type")
    private String type;
}
