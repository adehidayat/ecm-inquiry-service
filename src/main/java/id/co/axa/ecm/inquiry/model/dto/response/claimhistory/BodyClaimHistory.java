package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.axa.ecm.inquiry.model.dto.response.BaseResponseBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyClaimHistory extends BaseResponseBody {

	@JsonProperty("Customer")
	private CustomerClaimHistory Customer;
}
