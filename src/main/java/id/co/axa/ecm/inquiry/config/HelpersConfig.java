package id.co.axa.ecm.inquiry.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import id.co.axa.amfs.helpers.Helpers;

@Configuration
@EnableScheduling
public class HelpersConfig {

	@Bean
	public Helpers helpers() {
		return new Helpers();
	}
}
