package id.co.axa.ecm.inquiry.model.dto.response.claimhistorydetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ClaimHistoryDetailRes {

	@JsonProperty("Body")
	private BodyClaimHistoryDetail Body;
	
}
