package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPolicyAccount {
	private String policyEffectiveDTTM;
	private String paymentMethodCD;
	private String policyExpirationDTTM;
	private String initialLumpsumPremiumAMT;
	private List<HasLifePolicyChangeTransDetailsIn> hasLifePolicyChangeTransDetailsIn;
	private String policyReserveAmt;
	private List<HasApplicationDetailsIn> hasApplicationDetailsIn;
	private String paidToDT;
	private List<HasDetailsOfPolicyIn> hasDetailsOfPolicyIn;
	private String ePolicy;
	private String issueDt;
	private String sourceChannelCD;
	private List<HasLifePolicyTransactionDetailsIn> hasLifePolicyTransactionDetailsIn;
	private String policyRK;
	private List<HasPartyDetailsIn> hasPartyDetailsIn;
	private String currencyCD;
	private List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct;
	private String policyStatusCD;
	private String topupRegularModalPremium;
	private String statusChangeDT;
	private String terminationDt;
	private String policyNO;
	private String policyDueDt;
	private String premiumAMT;
	private String paymentModeCD;
	private List<HasDetailsOfRisksIn> hasDetailsOfRisksIn;
}
