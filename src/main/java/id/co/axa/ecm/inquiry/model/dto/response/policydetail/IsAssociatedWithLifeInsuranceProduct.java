package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class IsAssociatedWithLifeInsuranceProduct {

	private String planCd;
	private String insuranceProductRk;
	private List<HasPolicySegmentDetailsIn> hasPolicySegmentDetailsIn;
	private String planNM;
	private String insuranceProductCd;
}
