package id.co.axa.ecm.inquiry.model.dto.response.transactionstatusdetail;

import java.util.List;

import id.co.axa.ecm.inquiry.model.dto.response.BasePolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class PolicyTransactionStatusDetail extends BasePolicy {

	public List<HasPolicyAssessmentDetailsInTransactionStatusDetail> hasPolicyAssessmentDetailsIn;
}
