package id.co.axa.ecm.inquiry.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.axa.ecm.inquiry.model.entity.RfFundCode;

@Repository
public interface RfFundCodeRepository extends JpaRepository<RfFundCode, String>{

	Optional<RfFundCode> findByFundCode(String fundCode);
}
