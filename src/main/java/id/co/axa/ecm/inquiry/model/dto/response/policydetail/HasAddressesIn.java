package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasAddressesIn {
	private String emailAddress;
	private String mobilePhoneNO;
}
