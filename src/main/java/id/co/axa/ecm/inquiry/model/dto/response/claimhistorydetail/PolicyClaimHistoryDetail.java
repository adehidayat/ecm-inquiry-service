package id.co.axa.ecm.inquiry.model.dto.response.claimhistorydetail;

import java.util.List;

import id.co.axa.ecm.inquiry.model.dto.response.BasePolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class PolicyClaimHistoryDetail extends BasePolicy {

	private List<ClaimHistoryDetailHasPolicyAssessmentDetailsIn> hasPolicyAssessmentDetailsIn;
}
