package id.co.axa.ecm.inquiry.model.dto.response.paymentinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PaymentInfoRes {

	@JsonProperty("Body")
	private BodyPaymentInfo Body;
}
