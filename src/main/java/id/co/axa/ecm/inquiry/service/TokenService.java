package id.co.axa.ecm.inquiry.service;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import id.co.axa.ecm.inquiry.model.entity.TokenManagement;
import id.co.axa.ecm.inquiry.model.entity.Users;

public interface TokenService {

	Users findUsersByToken(String token);

	int isValidateToken(HttpServletRequest request) throws ParseException;

	TokenManagement findFirstByToken(String token);
}
