package id.co.axa.ecm.inquiry.model.dto.response.premiumbreakdown;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class IsAssociatedWithLifeInsuranceProduct {

	private List<HasPlanDetailsIn> hasPlanDetailsIn;
	private String saleEffectiveDt;
	private String planTypeCD;
	private String planCd;
	private String planClassCD;
	private String saleExpirationDt;
	private String insuranceProductCd;
}
