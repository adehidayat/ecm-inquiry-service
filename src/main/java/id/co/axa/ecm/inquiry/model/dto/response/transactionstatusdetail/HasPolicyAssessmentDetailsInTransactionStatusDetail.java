package id.co.axa.ecm.inquiry.model.dto.response.transactionstatusdetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPolicyAssessmentDetailsInTransactionStatusDetail {
	
	private String assmtSection;
	private String assmtStatus;
	private String lastUpdBy;
	private String assessmentClassType;
	private String assmtDesc;
	private String assmtRemark;
	private String assmtRefNo;
	private String assmtNo;
	private String assmtTypeCd;
	private String lastUpdDTTM;

}
