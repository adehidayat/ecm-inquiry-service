package id.co.axa.ecm.inquiry.service;

import id.co.axa.ecm.inquiry.model.dto.request.ApiCommonRequest;

public interface EipService {
	
	Object getDataPremiumBreakdown(ApiCommonRequest request);

}
