package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 3, 2021
 * Time: 2:28:33 PM
 */
@Getter
@Setter
@ToString()
public class CanBeIndividual {

	private String genderCD;
	private List<HasAddressesIn> hasAddressesIn = null;
	private String lastNM;
	private String firstNM;
	private String fullNM;
	private String birthPlace;
	private String birthDT;
}
