package id.co.axa.ecm.inquiry.model.dto.response.beneficiary;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasBeneficiaryXUoeDetailsIn {

	private String benefitDistributionPCT;
	private String relationshipToInsuredCD;
}
