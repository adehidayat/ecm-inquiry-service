package id.co.axa.ecm.inquiry.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 3:29:49 PM
 */
@Entity
@Data
@Table(name="token_management")
public class TokenManagement implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="expired_at")
    private String expiredAt;

    @Column(name="ip")
    private String ip;

    @Column(name="users_agent")
    private String usersAgent;

    @Column(name="token")
    private String token;

    @Column(name="maam_token")
    private String maamToken;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user")
    private Users users;
}
