package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasDetailsOfPolicyIn {
	private String gdbValue;
	private String dividendAmt;
	private String futurePremiumDpIntTypeCode;
	private String loanRepaymentAmt;
	private String futurePremiumDpAmt;
	private String loanIntTypeCd;
	private String dividendTypeCD;
}
