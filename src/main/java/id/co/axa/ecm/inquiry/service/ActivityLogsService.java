package id.co.axa.ecm.inquiry.service;

import id.co.axa.ecm.inquiry.model.entity.ActivityLogs;
import id.co.axa.ecm.inquiry.model.entity.Users;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 5:40:33 PM
 */
public interface ActivityLogsService {
	ActivityLogs insertLogsActivityApi(Users users, String activity);
}
