package id.co.axa.ecm.inquiry.model.dto.response.funddetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.ToString;
import lombok.Setter;

@Getter
@Setter
@ToString()
public class FundDetailRes {

	@JsonProperty("Body")
	private BodyFundDetail Body;
}
