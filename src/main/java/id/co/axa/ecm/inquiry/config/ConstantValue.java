package id.co.axa.ecm.inquiry.config;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 4:44:23 PM
 */
public class ConstantValue {
	
	private ConstantValue() {
		throw new IllegalStateException("Constant class");
	}

	public static final String API_BASE = "/api/v3";
	public static final String API_BASE_IMGS_HOS_DASHBOARD = API_BASE + "/imgs/hos";
	public static final String API_AUTH_BASE = API_BASE+"/auth";
	public static final String API_TOOLS_BASE = API_BASE+"/tools";
	public static final String GET_TOKEN_FULL = API_AUTH_BASE+"/get-token";
	public static final String RENEW_TOKEN_FULL = API_AUTH_BASE+"/token-renew";
	
	public static final String TOKEN_NAME = "Access-Token";
	public static final String AKUN_TIDAK_DITEMUKAN = "Akun Anda tidak ditemukan.";
	public static final String TIDAK_ADA_AKSES = "Anda tidak memiliki akses untuk mengakses url ini.";	
	
	public static final String ADMIN_URL = "/admin";
	public static final String LOGIN = ADMIN_URL+"/login";
	public static final String LOGOUT = ADMIN_URL+"/logout";
	public static final String DENIED = ADMIN_URL+"/denied";
	
	public static final String BLOCKED_IP_INFO = "Your IP address has been blocked by the system, please contact the admin";
	public static final String RELOGIN_INFO = "Sorry, your token was not found, please re-login first";
	public static final String RENEW_TOKEN_INFO = "Sorry your tokens don't match or expired, please renew your token";
	public static final String ACCOUNT_ALREADY_USED_INFO = "Sorry, your account has been used by someone else";
	public static final String STATUS_TIDAK_SESUAI_INFO= "Status yang anda masukkan tidak sesuai.";
	public static final String BELUM_TERDAFTAR_DI_SALES_GROUP_INFO = "Anda belum terdaftar disalesgroup, silahkan hubungi Admin";
	
	public static final String RESPONSE_STATUS_SUCCESS = "\"status\":\"1\"";
	public static final String RESPONSE_STATUS_FAILED = "\"status\":\"0\"";
	/*
    Customer Data
     */
	public static final String GET_CUSTOMER_DATA = "Get Customer Data By Policy Number";
	public static final String GET_CUSTOMER_PROFILE = "Get Customer Profile to Eip";
	public static final String GET_CUSTOMER_POLICY_DATA = "Get Customer Policy Detail to Eip";
	public static final String GET_CUSTOMER_PREMIUM_BREAKDOWN_DATA = "Get Customer Premium Breakdown Data to Eip";
	public static final String GET_CUSTOMER_INSURED_DETAIL_DATA = "Get Customer Insured Detail Data to Eip";
	public static final String GET_CUSTOMER_BENEFICIARY_DATA = "Get Customer Beneficiary Data to Eip";
	public static final String GET_CUSTOMER_FUND_DETAIL_DATA = "Get Customer Fund Detail Data to Eip";
	public static final String GET_TRANSACTION_STATUS_DATA = "Get Transaction Status to Eip";
	public static final String GET_CLAIM_HISTORY_DATA = "Get Claim History to Eip";
	public static final String GET_PAYMENT_INFO_DATA = "Get Payment Info to Eip";
	public static final String FUND_CODE_REFERENCE = "Get Fund Code Reference";
	public static final String All_FUND_CODE_REFERENCE = "Get All Fund Code Reference";
	public static final String CUSTOMER_DATA_NOT_FOUND = "Data Not Found";
}
