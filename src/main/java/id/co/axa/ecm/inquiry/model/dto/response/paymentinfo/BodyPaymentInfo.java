package id.co.axa.ecm.inquiry.model.dto.response.paymentinfo;

import com.fasterxml.jackson.annotation.JsonProperty;

import id.co.axa.ecm.inquiry.model.dto.response.BaseResponseBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyPaymentInfo extends BaseResponseBody {

	@JsonProperty("Policy")
	private PolicyPaymentInfo Policy;
}
