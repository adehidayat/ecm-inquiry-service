package id.co.axa.ecm.inquiry.model.dto.response.premiumbreakdown;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class Policy {

	private String policyNO;
	private List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct;
}
