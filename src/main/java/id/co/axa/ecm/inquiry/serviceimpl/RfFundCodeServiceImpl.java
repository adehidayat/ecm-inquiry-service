package id.co.axa.ecm.inquiry.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.ecm.inquiry.config.ConstantValue;
import id.co.axa.ecm.inquiry.config.Tools;
import id.co.axa.ecm.inquiry.model.dto.request.FundCodeReq;
import id.co.axa.ecm.inquiry.model.entity.RfFundCode;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.repository.RfFundCodeRepository;
import id.co.axa.ecm.inquiry.service.ActivityLogsService;
import id.co.axa.ecm.inquiry.service.FundCodeService;
import id.co.axa.ecm.inquiry.service.ResponseService;
import javassist.NotFoundException;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 19, 2021
 * Time: 11:31:23 AM
 */
@Service
public class RfFundCodeServiceImpl implements FundCodeService {
	
	@Autowired
	RfFundCodeRepository repo;
	
	@Autowired
	ResponseService response;
	
	@Autowired
	Tools tools;
	
	@Autowired
	ActivityLogsService activityLogsService;

	@Override
	public String getRfFundCodeByCode(String fundCode) {
		return response.apiSuccess(repo.findByFundCode(fundCode));
	}

	@Override
	public String getRfFundCodeByCode(String body, String accessToken, String regid, String requestId) throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.FUND_CODE_REFERENCE);
		FundCodeReq requestBody = tools.convertEncryptedStringToObject(body, FundCodeReq.class);
		RfFundCode data = repo.findByFundCode(requestBody.getFundCode()).orElseThrow(()-> new NotFoundException("Data Reference Fund Code Tidak ditemukan"));
		
		return response.apiSuccess(data);
	}

	@Override
	public String getAllRfFundCode(String accessToken, String regid, String requestId) throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.All_FUND_CODE_REFERENCE);
		return response.apiSuccess(repo.findAll());
	}

}
