package id.co.axa.ecm.inquiry.config.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.axa.ecm.inquiry.config.security.entity.BlockedIp;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 22, 2021
 * Time: 3:13:45 PM
 */
public interface BlockedIpRepository extends JpaRepository<BlockedIp, Long> {

	/**
	 * @param ip
	 * @param status
	 * @return
	 */
	BlockedIp findFirstByIpAndStatus(String ip,String status);
}
