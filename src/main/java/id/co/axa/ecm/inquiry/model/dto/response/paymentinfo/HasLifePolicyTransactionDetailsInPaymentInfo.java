package id.co.axa.ecm.inquiry.model.dto.response.paymentinfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasLifePolicyTransactionDetailsInPaymentInfo {

	private String transDT;
	private String paymentDueDT;
	private String paymentMethodCd;
	private String unitValuationDT;
	private String settledAMT;
	private String settledDT;
}
