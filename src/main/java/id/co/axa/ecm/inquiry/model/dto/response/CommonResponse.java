package id.co.axa.ecm.inquiry.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 4, 2021
 * Time: 10:35:04 AM
 * @param <T>
 */
@Getter
@Setter
@ToString()
public class CommonResponse<T> {
	
	@JsonProperty("Body")
	private T Body;

}
