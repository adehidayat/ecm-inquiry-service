package id.co.axa.ecm.inquiry.model.dto.response.insureddetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class InsuredDetailRes {

	@JsonProperty("Body")
	private BodyInsuredDetail Body;
}
