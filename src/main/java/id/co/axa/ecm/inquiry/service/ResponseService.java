package id.co.axa.ecm.inquiry.service;

public interface ResponseService {

	<T> String apiSuccess(T data);
	String apiError(String pesan);
	String apiSuccess(String pesan);
}
