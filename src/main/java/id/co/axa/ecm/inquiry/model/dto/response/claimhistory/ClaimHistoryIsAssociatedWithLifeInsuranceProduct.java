package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ClaimHistoryIsAssociatedWithLifeInsuranceProduct {

	private String planCd;
	private String key;
	private String productTypeCD;
}
