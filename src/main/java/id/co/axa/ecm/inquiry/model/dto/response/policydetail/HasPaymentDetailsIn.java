package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPaymentDetailsIn {

	public Object accountDetails;
}
