package id.co.axa.ecm.inquiry.config.security.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.ecm.inquiry.config.security.entity.BlockedIp;
import id.co.axa.ecm.inquiry.config.security.repository.BlockedIpRepository;
import id.co.axa.ecm.inquiry.config.security.service.BlockedIpService;

@Service
public class BlockedIpServiceImpl implements BlockedIpService{

	@Autowired
	BlockedIpRepository blockedIpRepository;
	
	@Override
	public BlockedIp findFirstByIpAndStatus(String ip, String status) {
		return blockedIpRepository.findFirstByIpAndStatus(ip, status);
	}

}
