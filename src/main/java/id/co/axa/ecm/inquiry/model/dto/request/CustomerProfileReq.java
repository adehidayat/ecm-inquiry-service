package id.co.axa.ecm.inquiry.model.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: Apr 29, 2021
 * Time: 5:26:41 PM
 */
@Getter
@Setter
@ToString()
public class CustomerProfileReq {

	private String policyNo;
	private String name;
}
