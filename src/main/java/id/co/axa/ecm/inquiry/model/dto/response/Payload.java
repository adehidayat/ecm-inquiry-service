package id.co.axa.ecm.inquiry.model.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Payload {
	
	private String name;
	private String address;
	private String mobilePhone;
	private String idDocumentType;
	private String idDocumentNo;
	private String gender;
	private String birthDate;
	private String email;
}
