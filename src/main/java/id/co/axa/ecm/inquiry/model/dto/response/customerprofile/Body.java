package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class Body {
	
	@JsonProperty("Customer")
	public Customer Customer;
	public String operation;
	public String transactionId;
	public String status;
	public String entity;
	public String service;
	public String appID;

}
