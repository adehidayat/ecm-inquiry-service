package id.co.axa.ecm.inquiry.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="activity_logs")
public class ActivityLogs {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String date;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_users")
    private Users users;

    private String role;

    private String activity;
}
