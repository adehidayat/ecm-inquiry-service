package id.co.axa.ecm.inquiry.config.security.service;

import id.co.axa.ecm.inquiry.config.security.entity.BlockedIp;

public interface BlockedIpService {

	BlockedIp findFirstByIpAndStatus(String ip,String status);
}
