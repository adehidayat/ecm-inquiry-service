package id.co.axa.ecm.inquiry.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 4:30:02 PM
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {                                    
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
        		.apiInfo(getApiInfo())
        		.select()
        		.apis(RequestHandlerSelectors.any())
        		.paths(PathSelectors.any())                          
        		.build();                                           
    }
	
	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Perfect Assistant AMFS-Notification")
				.description("Dokumentation API for ECM Inquiries")
				.contact(getContact()).license("Axa License")
				.licenseUrl("axa.co.id").version("1.0").build();
	}
	
	private Contact getContact() {
		return new Contact("AXA Service Indonesia", "axa.co.id", "ade.hidayat@axa.co.id");
	}
}
