package id.co.axa.ecm.inquiry.config.security.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.axa.ecm.inquiry.model.entity.CmsUsers;
import id.co.axa.ecm.inquiry.repository.CmsUsersRepository;

@Service
@Transactional
public class UserPrincipalDetailsServiceImpl implements UserDetailsService {

    private CmsUsersRepository cmsUsersRepository;

    public void userPrincipalDetailsServiceImpl(CmsUsersRepository cmsUsersRepository) {
        this.cmsUsersRepository = cmsUsersRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String s) {
        CmsUsers user = cmsUsersRepository.findByEmail(s);
        if(user==null){
            throw new UsernameNotFoundException("User doesn`t exist");
        }

        //Fetching User roles form DB.

        List<String> dbRoles=new ArrayList<>();
        dbRoles.add( user.getCmsPrivileges().getName().toUpperCase().replace(" ","_"));

        // pass user object and roles to LoggedUser
       return new UserPrincipal(user, dbRoles);
    }

}
