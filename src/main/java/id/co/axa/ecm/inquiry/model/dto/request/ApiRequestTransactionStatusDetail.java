package id.co.axa.ecm.inquiry.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiRequestTransactionStatusDetail extends ApiCommonRequest{
	
	private String assmtRefNo;
	private String lastUpdDTTM;

}
