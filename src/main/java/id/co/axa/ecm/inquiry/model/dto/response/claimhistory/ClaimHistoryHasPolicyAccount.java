package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import java.util.List;

import id.co.axa.ecm.inquiry.model.dto.response.BasePolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ClaimHistoryHasPolicyAccount extends BasePolicy {

	private List<ClaimHistoryHasDetailsOfClaimIn> hasDetailsOfClaimIn;
	private List<ClaimHistoryIsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct;
}
