package id.co.axa.ecm.inquiry.model.dto.response.transactionstatusdetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyTransactionStatusDetail {
	
	private PolicyTransactionStatusDetail policy;

}
