package id.co.axa.ecm.inquiry.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.ToString;

@Entity
@Table(name="RFFUNDCODE")
@ToString()
@Getter
public class RfFundCode {
	
	@Id
	@Column(name="FUND_CODE")
    private String fundCode;

    @Column(name="FUND_DESC")
    private String fundDesc;
    
    @Column(name="ACTIVE")
    private int isActive;

}
