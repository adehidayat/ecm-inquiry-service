package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasDetailsOfRisksIn {
	private String insuredID;
	private List<HasPersonalDetailsIn> hasPersonalDetailsIn;
}
