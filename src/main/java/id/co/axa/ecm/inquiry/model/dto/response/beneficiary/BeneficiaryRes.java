package id.co.axa.ecm.inquiry.model.dto.response.beneficiary;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BeneficiaryRes {

	@JsonProperty("Body")
	private BodyBeneficiary Body;
}
