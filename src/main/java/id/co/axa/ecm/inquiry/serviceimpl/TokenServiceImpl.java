package id.co.axa.ecm.inquiry.serviceimpl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.amfs.helpers.DateFormat;
import id.co.axa.ecm.inquiry.config.ConstantValue;
import id.co.axa.ecm.inquiry.model.entity.TokenManagement;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.repository.TokenRepository;
import id.co.axa.ecm.inquiry.service.TokenService;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 4:49:01 PM
 */
@Service
@Slf4j
public class TokenServiceImpl implements TokenService {

	@Autowired
	TokenRepository tokenRepository;
	
	@Override
	public Users findUsersByToken(String token) {
		TokenManagement tokenManagement = tokenRepository.findFirstByToken(token);
        return tokenManagement.getUsers();
	}

	@Override
	public int isValidateToken(HttpServletRequest request) throws ParseException {
		/**
         * 1 => oke
         * 2 => mismatch
         * 3 => not found
         */
        String token = request.getHeader(ConstantValue.TOKEN_NAME);

        TokenManagement tokenManagement = tokenRepository.findFirstByToken(token);
        if (tokenManagement!=null){
            String expiredAt = tokenManagement.getExpiredAt();
            Date dateExpired = DateFormat.strToDate(expiredAt);
            log.info("TOKEN MANAGEMENT "+ expiredAt + " " + dateExpired);

            if (this.isExpiredOrNot(dateExpired)){
            	log.info("TOKEN EXPIRED");
                return 2;
            }else{
                this.refresToken(tokenManagement);
                return 1;
            }
        }else{
            return 3;
        }
	}
	
	public boolean isExpiredOrNot(Date expiredDate) throws ParseException {
        Date now = new Date();
        long nowMil = DateFormat.dateToLong(now);
        long expiredToLong = DateFormat.dateToLong(expiredDate);
        if (expiredToLong<=nowMil){
        	log.info("EXPIRED TOKEN :");
        	log.info(""+ expiredToLong);
        	log.info("SEKARANG :");
        	log.info(""+nowMil);
            return true;
        }else {
            return false;
        }

    }
	
	public void refresToken(TokenManagement tokenManagement){
        String pola = "yyyy-MM-dd HH:mm:ss";
        String waktu = tokenManagement.getExpiredAt();
        Date waktuAwal = DateFormat.strToDate(waktu);

        Calendar calExpiredDate = DateFormat.tambahWaktuJam(waktuAwal,1); //tambah satu jam setiap nembak
        String newExpiredToken = DateFormat.tampilkanTanggalDanWaktu(calExpiredDate.getTime(), pola, null);

        tokenManagement.setExpiredAt(newExpiredToken);
        tokenRepository.save(tokenManagement);
    }

	@Override
	public TokenManagement findFirstByToken(String token) {
		return  tokenRepository.findFirstByToken(token);
	}

}
