package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class CanBeIndividual__1 {

	private Object hasPartyAccountDetailsIn;
}
