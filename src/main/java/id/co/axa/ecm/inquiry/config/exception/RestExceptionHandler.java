package id.co.axa.ecm.inquiry.config.exception;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.NoSuchElementException;

import javax.naming.NoPermissionException;
import javax.servlet.ServletException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.model.dto.response.crocodic.ResponseBase;
import javassist.NotFoundException;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 3, 2021
 * Time: 7:28:48 AM
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	Helpers helpers;

	private ResponseEntity<String> buildResponseEntity(String string) {
		return new ResponseEntity<>(string, HttpStatus.OK);
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	protected ResponseEntity<String> handleEntityNotFound(NoSuchElementException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		return buildResponseEntity(apiError.toString());
	}
	
	@ExceptionHandler(NoPermissionException.class)
	protected ResponseEntity<String> handleAccessDenied(NoPermissionException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(ParseException.class)
	protected ResponseEntity<String> handleParseException(ParseException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(NotFoundException.class)
	protected ResponseEntity<String> handleNotFoundException(NotFoundException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(NumberFormatException.class)
	protected ResponseEntity<String> handleNumberFormatException(NumberFormatException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(MalformedURLException.class)
	protected ResponseEntity<String> handleMalformedURLException(MalformedURLException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(JSONException.class)
	protected ResponseEntity<String> handleJsonException(JSONException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(BadRequest.class)
	protected ResponseEntity<String> handleBadRequestException(BadRequest ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(ServletException.class)
	protected ResponseEntity<String> handleServletException(ServletException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(IOException.class)
	protected ResponseEntity<String> handleIOException(IOException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(RuntimeException.class)
	protected ResponseEntity<String> handleRuntimeException(RuntimeException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
	
	@ExceptionHandler(ArithmeticException.class)
	protected ResponseEntity<String> handleArithmeticException(ArithmeticException ex) {
		ResponseBase apiError = new ResponseBase(ex.getMessage(),0);
		String json = helpers.convertToStringJsonString(apiError);
		return buildResponseEntity(helpers.encryptParameter(json));
	}
}
