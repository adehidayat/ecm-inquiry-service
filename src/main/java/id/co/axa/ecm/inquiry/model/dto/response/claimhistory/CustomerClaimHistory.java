package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class CustomerClaimHistory {

	private List<ClaimHistoryHasPolicyAccount> hasPolicyAccount;
}
