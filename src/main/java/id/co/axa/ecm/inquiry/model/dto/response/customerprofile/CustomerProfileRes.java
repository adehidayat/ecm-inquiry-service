package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: Apr 29, 2021
 * Time: 5:25:38 PM
 */
@Getter
@Setter
@ToString()
public class CustomerProfileRes {

	@JsonProperty("Body")
	private Body Body;
	
}
