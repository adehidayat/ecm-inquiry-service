package id.co.axa.ecm.inquiry.model.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class FundCodeReq {

	private String fundCode;
}
