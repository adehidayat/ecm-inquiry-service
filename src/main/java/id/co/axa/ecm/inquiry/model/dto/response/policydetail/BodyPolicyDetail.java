package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyPolicyDetail {
	@JsonProperty("Customer")
	private Customer Customer;
	private String operation;
	private String transactionId;
	private String status;
	private String entity;
	private String service;
	private String appID;
}
