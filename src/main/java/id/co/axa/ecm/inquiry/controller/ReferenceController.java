package id.co.axa.ecm.inquiry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.axa.ecm.inquiry.service.FundCodeService;
import javassist.NotFoundException;

@RestController
@RequestMapping("/reference")
public class ReferenceController {
	
	@Autowired
	FundCodeService fundCodeService;
	
	@PostMapping("/fundCode")
	public String getRfFundCodeByCode(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return fundCodeService.getRfFundCodeByCode(body,accessToken,regid,requestId);
	}
	
	@GetMapping("/fundCode")
	public String getAllRfFundCode(
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return fundCodeService.getAllRfFundCode(accessToken,regid,requestId);
	}

}
