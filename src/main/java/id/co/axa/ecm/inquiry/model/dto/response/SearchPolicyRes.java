package id.co.axa.ecm.inquiry.model.dto.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class SearchPolicyRes {

	private Long mdmId;
	private String name;
	private String policyNo;
	private String gender;
}
