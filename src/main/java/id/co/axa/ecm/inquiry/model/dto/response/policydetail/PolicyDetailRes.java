package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PolicyDetailRes {

	@JsonProperty("Body")
	private BodyPolicyDetail Body;
}
