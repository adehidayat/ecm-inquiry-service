package id.co.axa.ecm.inquiry.model.dto.response.beneficiary;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasBeneficiaryInformationIn {
	
	private List<CanBeIndividual> canBeIndividual;
	private List<HasBeneficiaryXUoeDetailsIn> hasBeneficiaryXUoeDetailsIn;

}
