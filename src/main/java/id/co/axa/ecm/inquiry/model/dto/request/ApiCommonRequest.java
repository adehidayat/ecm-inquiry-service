package id.co.axa.ecm.inquiry.model.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ApiCommonRequest {

	private String coreSystem;
	private String entity;
	private String policyNo;
	private String applicationId;
	private String requestId;
}
