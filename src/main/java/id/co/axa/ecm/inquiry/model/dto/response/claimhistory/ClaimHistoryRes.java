package id.co.axa.ecm.inquiry.model.dto.response.claimhistory;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ClaimHistoryRes {

	@JsonProperty("Body")
	private BodyClaimHistory Body;
}
