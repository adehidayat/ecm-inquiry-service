package id.co.axa.ecm.inquiry.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.ToString;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 3, 2021
 * Time: 8:28:14 AM
 */
@Entity
@Table(name="MDM_DATA")
@ToString()
@Getter
public class MdmData {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MDM_ID")
    private Long id;
	
	@Column(name="Gender")
    private String gender;

    @Column(name="Name")
    private String name;
    
    @Column(name="Birthdate")
    private String birthDate;

    @Column(name="Policy_Number")
    private String policyNumber;
    
    @Column(name="ID_Document_No")
    private String idDocumentNo;

    @Column(name="ID_Documents_Type")
    private String idDocumentType;

    @Column(name="Source_System")
    private String sourceSystem;
    
    @Column(name="Phone_Number")
    private String phoneNumber;


}
