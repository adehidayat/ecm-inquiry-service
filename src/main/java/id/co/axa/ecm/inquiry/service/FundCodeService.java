package id.co.axa.ecm.inquiry.service;

import javassist.NotFoundException;

public interface FundCodeService {

	String getRfFundCodeByCode(String fundCode);

	String getRfFundCodeByCode(String body, String accessToken, String regid, String requestId)  throws NotFoundException;

	String getAllRfFundCode(String accessToken, String regid, String requestId) throws NotFoundException;
}
