package id.co.axa.ecm.inquiry.model.dto.response.insureddetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPersonalDetailsIn {

	private String genderCD;
	private List<HasAddressesIn> hasAddressesIn;
	private String lastNM;
	private String firstNM;
	private String birthDT;
}
