package id.co.axa.ecm.inquiry.model.dto.response.paymentinfo;

import java.util.List;

import id.co.axa.ecm.inquiry.model.dto.response.BasePolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class PolicyPaymentInfo extends BasePolicy {
	
	public List<HasLifePolicyTransactionDetailsInPaymentInfo> hasLifePolicyTransactionDetailsIn;
	public String premiumAMT;
}
