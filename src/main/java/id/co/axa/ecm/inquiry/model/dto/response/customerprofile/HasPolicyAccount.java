package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPolicyAccount {

	private List<HasPartyDetailsIn> hasPartyDetailsIn;
	private String policyNO;
}
