package id.co.axa.ecm.inquiry.model.dto.response.beneficiary;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class CanBeIndividual {

	private String genderCD;
	private String lastNM;
	private String firstNM;
	private String birthPlace;
	private String birthDT;
	
}
