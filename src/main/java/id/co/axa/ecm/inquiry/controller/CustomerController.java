package id.co.axa.ecm.inquiry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.axa.ecm.inquiry.service.InquiryInformationService;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
	
	@Autowired
	InquiryInformationService policyService;

	@PostMapping("/SearchPolicy")
	public String searchCustumerProfileData(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getCustomerDataByPolicy(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/CustomerProfile")
	public String getCustumerProfileDetail(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getCustomerProfileData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/PolicyDetail")
	public String getCustumerPolicyDetail(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getCustomerPolicyData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/PremiumBreakdown")
	public String getPremiumBreakdown(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getPremiumBreakdownData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/InsuredDetail")
	public String getInsuredDetail(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getInsuredDetailData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/Beneficiary")
	public String getBeneficiary(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getBeneficiaryData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/FundDetail")
	public String getFundDetail(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getFundDetailData(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/TransactionStatus")
	public String getTransactionStatus(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getTransactionStatus(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/PaymentInfo")
	public String getPaymentInfo(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getPaymentInfo(body,accessToken,regid,requestId);
	}
	
	@PostMapping("/ClaimHistory")
	public String getClaimHistory(
			@RequestBody String body,
			@RequestHeader(value = "Access-Token") String accessToken,
			@RequestHeader(value = "regid") String regid,
			@RequestHeader(value = "Request-Id") String requestId) throws NotFoundException {
		return policyService.getClaimHistory(body,accessToken,regid,requestId);
	}

}
