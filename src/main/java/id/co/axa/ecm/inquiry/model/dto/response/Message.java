package id.co.axa.ecm.inquiry.model.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

	private String resultProperty;
	private String resultMessage;
}
