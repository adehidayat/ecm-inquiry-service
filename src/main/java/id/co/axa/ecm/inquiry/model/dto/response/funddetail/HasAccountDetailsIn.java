package id.co.axa.ecm.inquiry.model.dto.response.funddetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasAccountDetailsIn {

	private List<HasInvestmentFundDetailsIn> hasInvestmentFundDetailsIn = null;
}
