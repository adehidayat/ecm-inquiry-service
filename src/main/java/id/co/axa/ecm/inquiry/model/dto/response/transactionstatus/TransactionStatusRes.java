package id.co.axa.ecm.inquiry.model.dto.response.transactionstatus;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class TransactionStatusRes {

	@JsonProperty("Body")
	private BodyTransactionStatus Body;
}
