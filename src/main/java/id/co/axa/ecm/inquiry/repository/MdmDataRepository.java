package id.co.axa.ecm.inquiry.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.axa.ecm.inquiry.model.entity.MdmData;

@Repository
public interface MdmDataRepository extends JpaRepository<MdmData, Long> {
	
	@Query(value = "select d.id,d.name,d.policyNumber,d.phoneNumber,d.gender,d.idDocumentNo,d.idDocumentType,d.birthDate,d.sourceSystem from MdmData d where d.policyNumber=:policyNumber")
	List<Object[]> getDataByPolicyNumbers(String policyNumber);
	
	@Query(value = "select d.id,d.name,d.policyNumber,d.phoneNumber,d.gender,d.idDocumentNo,d.idDocumentType,d.birthDate,d.sourceSystem from MdmData d where name =:name")
	List<Object[]> getDataByName(String name);
	
	List<MdmData> findByPolicyNumber(String policyNumber);

}
