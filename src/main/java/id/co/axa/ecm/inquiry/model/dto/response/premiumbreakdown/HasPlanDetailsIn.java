package id.co.axa.ecm.inquiry.model.dto.response.premiumbreakdown;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPlanDetailsIn {

	private String planSI;
	private String premium;
	private String planStatus;
}
