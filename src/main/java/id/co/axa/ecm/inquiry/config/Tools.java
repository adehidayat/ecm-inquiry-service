package id.co.axa.ecm.inquiry.config;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.service.TokenService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 25, 2021
 * Time: 12:12:22 PM
 */
@Slf4j
@Service
public class Tools {

	@Autowired
	Helpers helpers;
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	ObjectMapper objectMapper;
	
	public String convertObjectToJsonSTring(Object obj) {
		ObjectWriter ow = new ObjectMapper().writer();
		String json = "";
		try {
			json = ow.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
		}
		return json;
	}
	
	public Users checkUsersIsExistByToken(String accessToken) throws NotFoundException {
		Users users = tokenService.findUsersByToken(accessToken);
		if (users == null)
			throw new NotFoundException(ConstantValue.AKUN_TIDAK_DITEMUKAN);
		return users;
	}
	
	public <T> T convertJsonObjectToObject(JSONObject jsonObject, Class<T> type) {
		return new Gson().fromJson(jsonObject.toString(), type);
	}
	
	public <T> T convertEncryptedCamelCaseStringToObject(String data, Class<T> type) {
		Gson gson = new GsonBuilder()
			    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
			    .create();
		return gson.fromJson(helpers.decryptParameter(data).toString(), type);
	}
	
	public <T> T convertEncryptedStringToObject(String data, Class<T> type) {
		return new Gson().fromJson(helpers.decryptParameter(data).toString(), type);
	}
	
	public <T> T convertStringToObject(String data, Class<T> type) {
		return new Gson().fromJson(data, type);
	}
	
	public <T> T convertObjectToMappingObject(Object object, Class<T> type) {
		ObjectWriter ow = new ObjectMapper().writer();
		String json = "";
		try {
			json = ow.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return new Gson().fromJson(json, type);
	}
	
	public static String periodeFromyearMonth(int year,int month){
        String yearStr = String.valueOf(year);
        String monthStr = String.valueOf(month);
        if (month<10){
            monthStr = "0"+monthStr;
        }
        return yearStr+monthStr;
    }
	
    public static Long longFromObjectString(Object key){
        return (key!=null)?Long.parseLong(key.toString()):Long.parseLong("0");
    }


}
