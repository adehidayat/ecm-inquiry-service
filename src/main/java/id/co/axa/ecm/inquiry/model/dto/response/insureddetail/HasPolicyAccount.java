package id.co.axa.ecm.inquiry.model.dto.response.insureddetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPolicyAccount {

	private String policyNO;
	private List<HasDetailsOfRisksIn> hasDetailsOfRisksIn;
}
