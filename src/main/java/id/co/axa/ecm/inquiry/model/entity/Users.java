package id.co.axa.ecm.inquiry.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 3:29:38 PM
 */
@Entity
@Table(name="users")
@Data
public class Users implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="email")
    private String email;

    @Column(name="name")
    private String name;

    @Column(name="sales_code")
    private String salesCode;

    @Column(name="role")
    private String role;

    @Column(name="photo")
    private String photo;

    @Column(name="regid")
    private String regid;

    private String platform;

    @Column(name="opt_code")
    private String optCode;

    @Column(name="has_edit_password")
    private Integer hasEditPassword;

    @Column(name="type")
    private String typeSales;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_users_group")
    private UsersGroup usersGroup;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_kategori_nonaktif")
    private KategoriNonaktif kategoriNonaktif;
}
