package id.co.axa.ecm.inquiry.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.model.dto.response.crocodic.ResponseBase;
import id.co.axa.ecm.inquiry.model.dto.response.crocodic.ResponseSchema;
import id.co.axa.ecm.inquiry.service.ResponseService;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 22, 2021
 * Time: 1:32:18 PM
 * @param <T>
 */
@Service
public class ResponseServiceImpl implements ResponseService{
	
	@Autowired
	Helpers helpers;
	
	@Override
	public <T> String apiSuccess(T data) {
		ResponseSchema<T> responseSchema = new ResponseSchema<>();
		responseSchema.setData(data);
		responseSchema.setMessage("success");
		responseSchema.setStatus(1);
		return encryptResponse(responseSchema);
	}
	
	@Override
	public String apiSuccess(String pesan) {
		ResponseBase responseSchema = new ResponseBase(pesan,1);
		return encryptResponse(responseSchema);
	}

	@Override
	public String apiError(String pesan) {
		ResponseBase responseTemplate = new ResponseBase(pesan,0);
		return encryptResponse(responseTemplate);
	}

	private String encryptResponse(ResponseBase responseTemplate) {
		String response = helpers.convertToStringJsonString(responseTemplate);
		response = helpers.encryptParameter(response);
		return response;
	}
}
