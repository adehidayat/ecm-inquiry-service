package id.co.axa.ecm.inquiry.model.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponseBody {
	
	private String operation;
	private String transactionId;
	private String status;
	private String entity;
	private String service;
	private String appID;
}
