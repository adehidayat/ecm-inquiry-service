package id.co.axa.ecm.inquiry.model.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class ApiRequestClaimHistoryDetail extends ApiCommonRequest{

	private String assmtRefNo;
	private String lastUpdDTTM;
	private String assessmentClassType;
}
