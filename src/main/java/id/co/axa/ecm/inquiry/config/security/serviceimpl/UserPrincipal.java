package id.co.axa.ecm.inquiry.config.security.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import id.co.axa.ecm.inquiry.model.entity.CmsUsers;

public class UserPrincipal implements UserDetails {

    private static final long serialVersionUID = 1L;
    private CmsUsers user;
    private List<String> roles;

    public 	UserPrincipal(CmsUsers user, List<String> roles){
        this.user=user;
        this.roles=roles;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities=new ArrayList<>();
        for (String role : roles) {
            String roleUsers = "ROLE_"+role;
            authorities.add(new SimpleGrantedAuthority(roleUsers));
        }

        return authorities;
    }

    public String getPassword() {
        return user.getPassword();
    }


    public String getUsername() {
        return user.getEmail();
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

}
