package id.co.axa.ecm.inquiry.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.axa.ecm.inquiry.model.entity.ActivityLogs;
import id.co.axa.ecm.inquiry.model.entity.Users;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 5:59:51 PM
 */
public interface ActivityLogsRepository extends JpaRepository<ActivityLogs, Long> {

    @Query(value = "select b from ActivityLogs b where b.activity like %:filter% or b.role like %:filter% or b.users.name like %:filter% or b.users.role like %:filter%")
    Page<ActivityLogs> filterQuery(String filter, Pageable pageable);

    @Query(value = "select b from ActivityLogs b where b.date >=:startDate and b.date<=:endDate and(b.activity like %:filter% or b.role like %:filter% or b.users.name like %:filter% or b.users.role like %:filter%)")
    Page<ActivityLogs> filterQuery(String filter,String startDate,String endDate, Pageable pageable);

    ActivityLogs findFirstByDateAndUsers(String date, Users users);

    @Query(value = "select b from ActivityLogs b where b.date >=:startDate and b.date<=:endDate")
    List<ActivityLogs> listAllFilterdByDate(String startDate,String endDate);
}
