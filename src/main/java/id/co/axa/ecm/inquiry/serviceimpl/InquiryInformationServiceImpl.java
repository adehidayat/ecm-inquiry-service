package id.co.axa.ecm.inquiry.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.config.ConstantValue;
import id.co.axa.ecm.inquiry.config.Tools;
import id.co.axa.ecm.inquiry.model.dto.request.ApiCommonRequest;
import id.co.axa.ecm.inquiry.model.dto.request.ApiRequestClaimHistoryDetail;
import id.co.axa.ecm.inquiry.model.dto.request.ApiRequestTransactionStatusDetail;
import id.co.axa.ecm.inquiry.model.dto.request.CustomerProfileReq;
import id.co.axa.ecm.inquiry.model.dto.response.SearchPolicyRes;
import id.co.axa.ecm.inquiry.model.dto.response.beneficiary.BeneficiaryRes;
import id.co.axa.ecm.inquiry.model.dto.response.claimhistory.ClaimHistoryHasDetailsOfClaimIn;
import id.co.axa.ecm.inquiry.model.dto.response.claimhistory.ClaimHistoryRes;
import id.co.axa.ecm.inquiry.model.dto.response.claimhistorydetail.ClaimHistoryDetailRes;
import id.co.axa.ecm.inquiry.model.dto.response.customerprofile.CustomerProfileRes;
import id.co.axa.ecm.inquiry.model.dto.response.funddetail.FundDetailRes;
import id.co.axa.ecm.inquiry.model.dto.response.insureddetail.InsuredDetailRes;
import id.co.axa.ecm.inquiry.model.dto.response.paymentinfo.PaymentInfoRes;
import id.co.axa.ecm.inquiry.model.dto.response.policydetail.PolicyDetailRes;
import id.co.axa.ecm.inquiry.model.dto.response.premiumbreakdown.PremiumBreakdownRes;
import id.co.axa.ecm.inquiry.model.dto.response.transactionstatus.HasPolicyAssessmentDetailsIn;
import id.co.axa.ecm.inquiry.model.dto.response.transactionstatus.TransactionStatusRes;
import id.co.axa.ecm.inquiry.model.dto.response.transactionstatusdetail.TransactionStatusDetailRes;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.repository.MdmDataRepository;
import id.co.axa.ecm.inquiry.repository.network.EipClientServiceRepository;
import id.co.axa.ecm.inquiry.service.ActivityLogsService;
import id.co.axa.ecm.inquiry.service.InquiryInformationService;
import id.co.axa.ecm.inquiry.service.ResponseService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InquiryInformationServiceImpl implements InquiryInformationService {
	
	@Autowired
	Tools tools;
	
	@Autowired
	ActivityLogsService activityLogsService;
	
	@Autowired
	EipClientServiceRepository eipClient;
	
	@Autowired
	MdmDataRepository repo;
	
	@Autowired
	Helpers helpers;
	
	@Autowired
	ResponseService response;

	@Override
	public String getCustomerDataByPolicy(String body, String accessToken, String regid, String requestId) throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_DATA);
		CustomerProfileReq requestBody = tools.convertEncryptedStringToObject(body, CustomerProfileReq.class);


		List<Object[]> queryResult = new ArrayList<Object[]>();
		if(!requestBody.getPolicyNo().isEmpty()) {
			queryResult = repo.getDataByPolicyNumbers(requestBody.getPolicyNo());
		}
		if(!requestBody.getName().isEmpty() && requestBody.getPolicyNo().isEmpty()) {
			queryResult = repo.getDataByName(requestBody.getName());
		}
		
		if(queryResult.isEmpty())
			return response.apiError(ConstantValue.CUSTOMER_DATA_NOT_FOUND);
		
		
		List<SearchPolicyRes> res = new ArrayList<>();
		for(Object[] dataMdm : queryResult) {		
			/**
			 *0. id
			 *1. name
			 *2. policy number
			 *3. phone number
			 *4. gender
			 *5. idDocumentNo
			 *6. idDocumentType
			 *7. birthDate
			 *8. sourceSystem
			 */
			@SuppressWarnings("unchecked")
			List<Object> queryRes = tools.convertObjectToMappingObject(dataMdm, ArrayList.class);
//			System.out.println(queryRes.get(0).toString());
			SearchPolicyRes result = new SearchPolicyRes();
			result.setMdmId(Tools.longFromObjectString(dataMdm[0]));
			result.setName(queryRes.get(1).toString());
			result.setGender(queryRes.get(4).toString());
			result.setPolicyNo(queryRes.get(2).toString());
			res.add(result);
		}
//		SearchPolicyRes res = new SearchPolicyRes();
//		res.setName(data.getName());
//		res.setGender(data.getGender());
//		res.setPolicyNo(data.getPolicyNumber());
		return response.apiSuccess(res);
	}

	@Override
	public String getCustomerPolicyData(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_POLICY_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		Object res = eipClient.getEipPolicyDetail(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		PolicyDetailRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, PolicyDetailRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getCustomerProfileData(String body, String accessToken, String regid, String requestId) throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_PROFILE);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);

		Object res = eipClient.getEipCustomerProfile(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		CustomerProfileRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, CustomerProfileRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getPremiumBreakdownData(String body, String accessToken, String regid, String requestId) throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_PREMIUM_BREAKDOWN_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipPremiumBreakdown(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		
		PremiumBreakdownRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, PremiumBreakdownRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getInsuredDetailData(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_PREMIUM_BREAKDOWN_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipInsuredDetail(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		

		InsuredDetailRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, InsuredDetailRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getBeneficiaryData(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_BENEFICIARY_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipBeneficiary(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		
		BeneficiaryRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, BeneficiaryRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getFundDetailData(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_CUSTOMER_BENEFICIARY_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipFundDetail(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		log.info("response from eip "+stringNoWhiteSPace);
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		FundDetailRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, FundDetailRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getTransactionStatus(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_TRANSACTION_STATUS_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipTransactionStatus(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		
		log.info("response from eip "+stringNoWhiteSPace);
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		
		TransactionStatusRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, TransactionStatusRes.class);
		int i = 0;
		for(HasPolicyAssessmentDetailsIn status : responseConvert.getBody().getPolicy().getHasPolicyAssessmentDetailsIn()) {
			if(!status.getAssmtStatus().equalsIgnoreCase("09")) {
				ApiRequestTransactionStatusDetail requestTransactionDetail = new ApiRequestTransactionStatusDetail();
				requestTransactionDetail.setApplicationId(requestBody.getApplicationId());
				requestTransactionDetail.setAssmtRefNo(status.getAssmtRefNo());
				requestTransactionDetail.setCoreSystem(requestBody.getCoreSystem());
				requestTransactionDetail.setEntity(requestBody.getEntity());
				requestTransactionDetail.setLastUpdDTTM(status.getLastUpdDTTM());
				requestTransactionDetail.setPolicyNo(requestBody.getPolicyNo());
				requestTransactionDetail.setRequestId(requestBody.getRequestId());
				
				Object resTransactionStatusDetail = eipClient.getEipTransactionStatusDetail(requestTransactionDetail);
				stringNoWhiteSPace = tools.convertObjectToJsonSTring(resTransactionStatusDetail).replaceAll("\\s", "");
				
//				log.info("response from eip transaction status detail "+stringNoWhiteSPace);
				TransactionStatusDetailRes responseConvertTransactionStatusDetail = tools.convertStringToObject(stringNoWhiteSPace, TransactionStatusDetailRes.class);
				responseConvert.getBody().getPolicy().getHasPolicyAssessmentDetailsIn().get(i).setRemarks(responseConvertTransactionStatusDetail.getBody().getPolicy().getHasPolicyAssessmentDetailsIn().get(0).getAssmtDesc());
			}
			i++;
		}
		
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}
	
	@Override
	public String getClaimHistory(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_TRANSACTION_STATUS_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipClaimHistory(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		
		log.info("response from eip "+stringNoWhiteSPace);
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		
		ClaimHistoryRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, ClaimHistoryRes.class);
		
		int i = 0;
		for(ClaimHistoryHasDetailsOfClaimIn detailClaim : responseConvert.getBody().getCustomer().getHasPolicyAccount().get(0).getHasDetailsOfClaimIn()) {
			int numberChar = detailClaim.getClaimReferenceID().length();
//			log.info("jumlah character no ref  "+ numberChar);
			int startFrom = numberChar - 7;
			String refNo = detailClaim.getClaimReferenceID().substring(startFrom);
//			log.info(refNo);
			if(!detailClaim.getClaimStatusCD().equalsIgnoreCase("C")) {
				
				ApiRequestClaimHistoryDetail requestClaimHistoryDetail = new ApiRequestClaimHistoryDetail();
				requestClaimHistoryDetail.setApplicationId(requestBody.getApplicationId());
				requestClaimHistoryDetail.setAssmtRefNo(refNo);
				requestClaimHistoryDetail.setCoreSystem(requestBody.getCoreSystem());
				requestClaimHistoryDetail.setEntity(requestBody.getEntity());
				requestClaimHistoryDetail.setLastUpdDTTM("");
				requestClaimHistoryDetail.setPolicyNo(requestBody.getPolicyNo());
				requestClaimHistoryDetail.setRequestId(requestBody.getRequestId());
				requestClaimHistoryDetail.setAssessmentClassType("CLM");
				
				Object resTransactionStatusDetail = eipClient.getEipClaimHistoryDetail(requestClaimHistoryDetail);
				stringNoWhiteSPace = tools.convertObjectToJsonSTring(resTransactionStatusDetail);
			
				log.info("response from eip claim detail "+stringNoWhiteSPace);
				ClaimHistoryDetailRes responseConvertClaimHistoryDetail = tools.convertStringToObject(stringNoWhiteSPace, ClaimHistoryDetailRes.class);
				String detail = responseConvertClaimHistoryDetail.getBody().getPolicy().getHasPolicyAssessmentDetailsIn().get(0).getAssmtDesc();
				responseConvert.getBody().getCustomer().getHasPolicyAccount().get(0).getHasDetailsOfClaimIn().get(i).setRemark(detail);
			}
			int indexPlanCd =Integer.valueOf(responseConvert.getBody().getCustomer().getHasPolicyAccount().get(0).getIsAssociatedWithLifeInsuranceProduct().get(i).getKey());
			String planCdIsAssociatedWithLifeInsuranceProduct = responseConvert.getBody().getCustomer().getHasPolicyAccount().get(0).getIsAssociatedWithLifeInsuranceProduct().get(indexPlanCd).getPlanCd();
			responseConvert.getBody().getCustomer().getHasPolicyAccount().get(0).getHasDetailsOfClaimIn().get(i).setPlanCd(planCdIsAssociatedWithLifeInsuranceProduct);
			i++;
		}
		
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

	@Override
	public String getPaymentInfo(String body, String accessToken, String regid, String requestId)
			throws NotFoundException {
		Users users = tools.checkUsersIsExistByToken(accessToken);
		activityLogsService.insertLogsActivityApi(users, ConstantValue.GET_PAYMENT_INFO_DATA);
		ApiCommonRequest requestBody = tools.convertEncryptedStringToObject(body, ApiCommonRequest.class);
		
		Object res = eipClient.getEipPaymentInfo(requestBody);
		
		String stringNoWhiteSPace = tools.convertObjectToJsonSTring(res).replaceAll("\\s", "");
		log.info("response from eip "+stringNoWhiteSPace);
		
		if(stringNoWhiteSPace.contains(ConstantValue.RESPONSE_STATUS_FAILED)) {
			return response.apiSuccess(res);
		}
		PaymentInfoRes responseConvert = tools.convertStringToObject(stringNoWhiteSPace, PaymentInfoRes.class);
		log.info("mapping result "+responseConvert);
		return response.apiSuccess(responseConvert);
	}

}
