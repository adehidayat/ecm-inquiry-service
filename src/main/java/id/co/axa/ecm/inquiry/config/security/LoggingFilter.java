package id.co.axa.ecm.inquiry.config.security;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.config.ConstantValue;
import id.co.axa.ecm.inquiry.config.security.entity.BlockedIp;
import id.co.axa.ecm.inquiry.config.security.service.BlockedIpService;
import id.co.axa.ecm.inquiry.model.entity.TokenManagement;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.service.TokenService;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LoggingFilter extends GenericFilterBean {

	@Autowired
	private TokenService tokenService;

	@Autowired
	private BlockedIpService blockedIpService;

	@Autowired
	private Helpers helpers;

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse response, FilterChain chain) {
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		String currentUrl = request.getRequestURI();

		try {

			// API TOKEN AKSES
			if (currentUrl.contains(ConstantValue.API_BASE) && !currentUrl.contains(ConstantValue.GET_TOKEN_FULL)
					&& !currentUrl.contains(ConstantValue.API_TOOLS_BASE)
					&& !currentUrl.contains(ConstantValue.RENEW_TOKEN_FULL)) {
				String ip = request.getRemoteAddr();
				BlockedIp blockedIp = blockedIpService.findFirstByIpAndStatus(ip, "blocked");
				if (blockedIp != null) {
					response(response, 0, ConstantValue.BLOCKED_IP_INFO, HttpStatus.OK.value());
				} else {
					validatedToken(response, request, currentUrl);
				}
			}
			chain.doFilter(servletRequest, response);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	private void validatedToken(ServletResponse response, final HttpServletRequest request, String currentUrl)
			throws IOException, ParseException {
		String token = request.getHeader(ConstantValue.TOKEN_NAME);
		String regid = request.getHeader("regid");
		int validToken = 3;

		validToken = tokenService.isValidateToken(request);

		if (validToken == 2) {

			response(response, validToken, ConstantValue.RENEW_TOKEN_INFO, HttpStatus.UNAUTHORIZED.value());

		} else if (validToken == 3) {

			response(response, validToken, ConstantValue.RELOGIN_INFO, HttpStatus.FORBIDDEN.value());

		} else {
			TokenManagement tokenManagement = tokenService.findFirstByToken(token);
			if (!currentUrl.contains(ConstantValue.API_AUTH_BASE)) {
				checkUsersIsLogin(response, regid, tokenManagement);
			}
		}
	}

	private void checkUsersIsLogin(ServletResponse response, String regid, TokenManagement tokenManagement)
			throws IOException {
		Users users = tokenManagement.getUsers();
		if (users == null) {
			response(response, 3, ConstantValue.RELOGIN_INFO, HttpStatus.FORBIDDEN.value());
		} else {
			if (users.getRegid() != null) {
				checkUsersRegid(response, regid, users);
			} else {
				response(response, 3, ConstantValue.RELOGIN_INFO, HttpStatus.FORBIDDEN.value());
			}

		}
	}

	private void checkUsersRegid(ServletResponse response, String regid, Users users) throws IOException {
		// harusnya tidak sama
		if (!users.getRegid().equals(regid)) {
			response(response, 4, ConstantValue.ACCOUNT_ALREADY_USED_INFO, HttpStatus.FORBIDDEN.value());
		}
	}

	private void response(ServletResponse response, int code, String pesan, int httpStatuscode) throws IOException {
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		httpServletResponse.setStatus(httpStatuscode);
		httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
		JSONObject resp = new JSONObject();

		resp.put("status", code);
		resp.put("message", pesan);
		String respon = helpers.encryptParameter(resp.toString());
		mapper.writeValue(httpServletResponse.getWriter(), respon);

	}

}
