package id.co.axa.ecm.inquiry.model.dto.response.transactionstatus;

import id.co.axa.ecm.inquiry.model.dto.response.BaseResponseBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyTransactionStatus extends BaseResponseBody{
	
	private PolicyTransactionStatus policy;

}
