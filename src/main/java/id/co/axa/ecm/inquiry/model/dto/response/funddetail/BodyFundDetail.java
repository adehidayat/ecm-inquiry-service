package id.co.axa.ecm.inquiry.model.dto.response.funddetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyFundDetail {

	private String operation;
	private String transactionId;
	private String status;
	private String entity;
	private String service;
	private Policy policy;
	private String appID; 
}
