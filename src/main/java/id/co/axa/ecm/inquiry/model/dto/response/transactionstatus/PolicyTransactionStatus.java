package id.co.axa.ecm.inquiry.model.dto.response.transactionstatus;

import java.util.List;

import id.co.axa.ecm.inquiry.model.dto.response.BasePolicy;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class PolicyTransactionStatus extends BasePolicy {

	private List<HasPolicyAssessmentDetailsIn> hasPolicyAssessmentDetailsIn;
}
