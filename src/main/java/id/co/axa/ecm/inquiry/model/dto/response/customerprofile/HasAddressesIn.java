package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasAddressesIn {

	private String emailAddress;
	private String addressLine4;
	private String addressLine3;
	private String addressLine2;
	private String mobilePhoneNO;
	private String addressLine1;
}
