package id.co.axa.ecm.inquiry.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import id.co.axa.ecm.inquiry.config.ConstantValue;
import id.co.axa.ecm.inquiry.config.security.serviceimpl.UserPrincipalDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	private UserPrincipalDetailsServiceImpl userPrincipalDetailsService;
	
	public SecurityConfiguration(UserPrincipalDetailsServiceImpl userPrincipalDetailsService) {
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
//                .addFilterBefore(loggingFilter, AnonymousAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(ConstantValue.LOGIN).permitAll()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers("/swagger*").permitAll()
                .antMatchers("/assets*").permitAll()
                .antMatchers("/storage*").permitAll()
                .antMatchers("/admin/**").authenticated()
                .antMatchers("/management/**").hasRole("SUPER_ADMIN")
                .antMatchers("/management/**").authenticated()
                .and()
                .formLogin()
//                .successHandler(authenticationSuccessHandler())
//                .failureHandler(new CustomAuthenticationFailureHandler())
                .loginPage(ConstantValue.LOGIN)
                .usernameParameter("email")
                .passwordParameter("password")
                .loginProcessingUrl(ConstantValue.LOGIN)
//                .defaultSuccessUrl("/admin/home", true)
//                .failureUrl("/admin/login?error=true")
                .and()
                .logout()
//                .addLogoutHandler(logoutHandler)
                .logoutUrl(ConstantValue.LOGOUT)
//                .logoutSuccessHandler(logoutSuccessHandler())
                .deleteCookies("JSESSIONID")
                .and()
                .exceptionHandling().accessDeniedPage(ConstantValue.DENIED);
        http.headers().frameOptions().disable();

    }
	
	@Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers("/swagger-resources/**",
                        "/swagger-ui.html",
                        "/v2/api-docs",
                        "/webjars/**",
                        "/resources/**", "/static/**", "/css/**", "/js/**", "/storage/**", "/assets/**");
        
    }
	
	@Bean
    DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(this.userPrincipalDetailsService);

        return daoAuthenticationProvider;
    }
	
	@Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	private static final String[] AUTH_WHITELIST = {
            // -- Swagger UI v2
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**"
            // other public endpoints of your API may be appended to this array
    };

}
