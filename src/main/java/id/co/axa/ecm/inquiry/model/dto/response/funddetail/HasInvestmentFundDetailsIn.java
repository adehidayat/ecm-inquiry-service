package id.co.axa.ecm.inquiry.model.dto.response.funddetail;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasInvestmentFundDetailsIn {

	private String totalUnits;
	private String fundAllocation;
	private String guarLifetimeRt;
	private String totalFundAmount;
	private String investmentFundCd;
	private String guarInvestmentRt;
	private String unitPrice;
	private String navDt;
}
