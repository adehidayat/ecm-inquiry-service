package id.co.axa.ecm.inquiry.model.dto.response.transactionstatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPolicyAssessmentDetailsIn {

	private String assmtSection;
	private String assmtStatus;
	private String assessmentClassType;
	private String assmtRemark;
	private String assmtRefNo;
	private String assmtTypeCd;
	private String lastUpdDTTM;
	private String remarks="";
}
