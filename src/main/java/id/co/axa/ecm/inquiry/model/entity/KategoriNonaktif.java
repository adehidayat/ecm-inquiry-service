package id.co.axa.ecm.inquiry.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 3:29:17 PM
 */
@Data
@Entity
@Table(name="kategori_nonaktif")
public class KategoriNonaktif implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 501150325104496094L;

	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;

	 @Column(name="created_at")
	 private String createdAt;

	 @Column(name="nama_kategori")
	 private String namaKategori;
}
