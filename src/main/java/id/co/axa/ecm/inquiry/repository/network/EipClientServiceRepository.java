package id.co.axa.ecm.inquiry.repository.network;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.axa.ecm.inquiry.model.dto.request.ApiCommonRequest;
import id.co.axa.ecm.inquiry.model.dto.request.ApiRequestClaimHistoryDetail;
import id.co.axa.ecm.inquiry.model.dto.request.ApiRequestTransactionStatusDetail;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Date: May 3, 2021
 * Time: 2:09:57 PM
 */
@FeignClient("EIP-CLIENT-SERVICE")
public interface EipClientServiceRepository {
	
	@PostMapping("/policy_service/api/v1/Customer-Profile")
	public Object getEipCustomerProfile(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Policy")
	public Object getEipPolicyDetail(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Premium-Breakdown")
	public Object getEipPremiumBreakdown(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Insured-Detail")
	public Object getEipInsuredDetail(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Beneficiary")
	public Object getEipBeneficiary(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Fund-Detail")
	public Object getEipFundDetail(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Transaction-Status")
	public Object getEipTransactionStatus(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Transaction-Status-Detail")
	public Object getEipTransactionStatusDetail(@RequestBody ApiRequestTransactionStatusDetail req);
	
	@PostMapping("/policy_service/api/v1/Payment-Info")
	public Object getEipPaymentInfo(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Claim-History")
	public Object getEipClaimHistory(@RequestBody ApiCommonRequest req);
	
	@PostMapping("/policy_service/api/v1/Claim-History-Detail")
	public Object getEipClaimHistoryDetail(@RequestBody ApiRequestClaimHistoryDetail req);

}
