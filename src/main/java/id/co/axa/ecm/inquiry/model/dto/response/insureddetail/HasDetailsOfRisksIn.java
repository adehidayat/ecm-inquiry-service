package id.co.axa.ecm.inquiry.model.dto.response.insureddetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasDetailsOfRisksIn {

	private String insuredID;
	private String dependNo;
	private List<HasPersonalDetailsIn> hasPersonalDetailsIn;
	private String agrRk;
}
