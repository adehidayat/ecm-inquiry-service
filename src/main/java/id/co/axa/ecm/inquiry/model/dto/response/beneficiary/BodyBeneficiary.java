package id.co.axa.ecm.inquiry.model.dto.response.beneficiary;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyBeneficiary {
	
	private String operation;
	private String transactionId;
	private String status;
	private String entity;
	private String service;
	@JsonProperty("Policy")
	private Policy policy;
	private String appID;

}
