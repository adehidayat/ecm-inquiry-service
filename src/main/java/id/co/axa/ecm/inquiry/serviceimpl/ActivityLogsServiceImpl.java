package id.co.axa.ecm.inquiry.serviceimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.axa.amfs.helpers.DateFormat;
import id.co.axa.amfs.helpers.Helpers;
import id.co.axa.ecm.inquiry.model.entity.ActivityLogs;
import id.co.axa.ecm.inquiry.model.entity.Users;
import id.co.axa.ecm.inquiry.repository.ActivityLogsRepository;
import id.co.axa.ecm.inquiry.service.ActivityLogsService;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 5:42:47 PM
 */
@Service
@Transactional
public class ActivityLogsServiceImpl implements ActivityLogsService {
	
	@Autowired
    private ActivityLogsRepository repo;
	
	@Autowired
	Helpers helpers;

	@Override
	 public ActivityLogs insertLogsActivityApi(Users users,String activity){
        if (users!=null){
            String date = DateFormat.currentDate();
            ActivityLogs activityLogs = repo.findFirstByDateAndUsers(date,users);
            if (activityLogs==null){
                activityLogs = new ActivityLogs();
            }
            activityLogs.setRole(users.getRole());
            activityLogs.setDate(date);
            activityLogs.setActivity(activity);
            activityLogs.setUsers(users);
            return repo.save(activityLogs);
        }else{
            return null;
        }
    }

}
