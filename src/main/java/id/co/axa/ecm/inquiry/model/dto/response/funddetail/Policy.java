package id.co.axa.ecm.inquiry.model.dto.response.funddetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class Policy {

	private String processedDttm;
	private String policyNO;
	private List<HasAccountDetailsIn> hasAccountDetailsIn;
	private String key;
}
