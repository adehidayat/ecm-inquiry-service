package id.co.axa.ecm.inquiry.model.dto.response.customerprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPartyDetailsIn {

	private CanBeIndividual__1 canBeIndividual;
	private String servicingCD;
	private String producingCD;
}
