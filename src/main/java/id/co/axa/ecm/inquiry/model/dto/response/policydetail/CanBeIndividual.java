package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class CanBeIndividual {
	private String genderCD;
	private List<HasAddressesIn> hasAddressesIn ;
	private String firstNM;
	private String socialSecurityNO;
	private String birthPlace;
	private String birthDT;
}
