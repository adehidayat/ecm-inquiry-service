package id.co.axa.ecm.inquiry.model.dto.response.policydetail;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class HasPersonalDetailsIn {

	private String genderCD;
	private List<HasAddressesIn__1> hasAddressesIn;
	private String lastNM;
	private String firstNM;
	private String birthPlace;
	private String birthDT;
}
