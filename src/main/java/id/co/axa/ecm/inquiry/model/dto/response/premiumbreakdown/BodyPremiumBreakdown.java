package id.co.axa.ecm.inquiry.model.dto.response.premiumbreakdown;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class BodyPremiumBreakdown {

	private String operation;
	private String transactionId;
	private String status;
	@JsonProperty("Policy")
	private Policy Policy;
	private String entity;
	private String service;
	private String appID;
}
