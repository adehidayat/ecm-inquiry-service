package id.co.axa.ecm.inquiry.model.dto.response.transactionstatusdetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString()
public class TransactionStatusDetailRes {

	@JsonProperty("Body")
	private BodyTransactionStatusDetail Body;
}
