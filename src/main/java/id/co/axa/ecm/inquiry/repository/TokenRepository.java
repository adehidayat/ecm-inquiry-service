package id.co.axa.ecm.inquiry.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.axa.ecm.inquiry.model.entity.TokenManagement;

/**
 * Created by Spring Tool Suite.
 * @author Ade Hidayat
 * Email: ade.hidayat@axa.co.id
 * Date: Mar 19, 2021
 * Time: 4:42:08 PM
 */
public interface TokenRepository extends JpaRepository<TokenManagement, Long> {
    TokenManagement findFirstByToken(String token);

    void deleteByToken(String token);
}
